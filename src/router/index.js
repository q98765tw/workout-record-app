import Vue from 'vue'
import VueRouter from 'vue-router'
import homepage from '../views/homepage.vue'
import create from '../views/create.vue'
import training from '../views/training.vue'
import userlogin from '../views/userLogin.vue'

Vue.use(VueRouter)

const routes = [

  {
    path: '/',
    name: 'userlogin',
    component: userlogin
  },
  {
    path: '/homepage',
    name: 'homepage',
    component: homepage
  },
  {
    path: '/training',
    name: 'training',
    component: training
  },
  {
    path: '/create',
    name: 'create',
    component: create
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },


]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
