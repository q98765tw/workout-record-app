module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '/workout-record-app/' : '/',
  };